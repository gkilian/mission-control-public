import PropTypes from 'prop-types'
import React from 'react';
import { Provider } from 'react-redux'
import { BrowserRouter as Router, Route } from "react-router-dom";

import Compare from './views/pages/compare';
import CompareDrawer from './views/components/compare-drawer';
import Dashboard from './views/pages/dashboard';
import Header from './views/components/header';
import Navigation from './views/components/navigation';
import Profile from './views/pages/profile';

const App = ({ store }) => {
  return (
    <Provider store={store}>
      <Router>
        <div className="mission-control">
          <Header />
          <Navigation />
          <div className="content">
            <Route exact path="/" component={Dashboard} />
            <Route exact path="/heros" component={Dashboard} />
            <Route exact path="/Villains" component={Dashboard} />
            <Route exact path="/character/:id" component={Profile} />
            <Route exact path="/compare" component={Compare} />
          </div>
          <CompareDrawer />
        </div>
      </Router>
    </Provider>
  );
}

App.propTypes = {
  store: PropTypes.object.isRequired
}

export default App;
