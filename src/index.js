import React from 'react';
import ReactDOM from 'react-dom'
import { createStore, applyMiddleware } from 'redux'
import thunk from 'redux-thunk';

import reducer from './store/reducers';

import App from './App';

import './app.scss';

const store = createStore(reducer, applyMiddleware(thunk))

ReactDOM.render(<App store={store} />, document.getElementById('root'));
