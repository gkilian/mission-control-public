import PropTypes from 'prop-types'
import React from 'react';

import { CharacterCard } from '../character-display';

import './character-grid.scss';

/**
 * CharacterGrid - Renders a grid of CharacterProfiles
 *
 * @param {Object} props - React props
 * @param {Array} props.characters - Array of characters
 *
 * @returns {ReactElement} <div>
 */
const CharacterGrid = ({ characters }) => {
  return (
    <div className="character-grid">
      {!!characters.length && characters.map((character) => {
        return (
          <CharacterCard
            {...character}
            key={character.id}
          />
        );
      })}
    </div>
  )
}

CharacterGrid.defaultProps ={
  characters: []
}

CharacterGrid.propTypes = {
  characters: PropTypes.arrayOf(PropTypes.shape(CharacterCard.propTypes))
}

export default CharacterGrid;
