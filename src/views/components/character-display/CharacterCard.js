import classNames from 'classnames';
import PropTypes from 'prop-types'
import React, { PureComponent } from 'react';
import { connect } from "react-redux";
import { Link } from "react-router-dom";

import { addCompareCharacter } from '../../../store/actions';

import './character-display.scss';

/**
 * CharacterCard - Renders a limited character profile in a card form
 *
 * @param {Object} props - React props
 * @param {string} [props.id] - character id
 * @param {Object} [props.image] - character profile image
 * @param {string} [props.name] - character name
 * @param {Object} [props.work] - character occupation
 *
 * @todo - Lazy-load character profile image
 *
 * @returns {ReactElement} <div>
 */
class CharacterCard extends PureComponent{
  constructor(props) {
    super(props);

    this.handleCompare = this.handleCompare.bind(this);
  }

  handleCompare() {
    const { dispatch } = this.props;

    dispatch(addCompareCharacter(this.props))
  }

  render() {
    const {
      id,
      name,
      image,
      selectedHero,
      selectedVillain,
      work
    } = this.props;

    const heroId = selectedHero[0] && selectedHero[0].id;
    const villainId = selectedVillain[0] && selectedVillain[0].id;
    const isSelected = heroId === id || villainId === id;

    return (
      <div className={classNames('character-card', {'character-card--selected': isSelected})}>
        <Link to={`/character/${id}`}>
          <div className="character-card__image">
            <img alt={name} src={image.url} />
            <div className="character-card__check">
              <img alt="checked" src={`${process.env.PUBLIC_URL}/check.svg`} />
            </div>
          </div>
          <h3 className="character-card__name">{name}</h3>
          <p className="character-card__occupation">{work.occupation}</p>
        </Link>
        <button
          type="button"
          className="character-card__compare btn"
          onClick={this.handleCompare}>Select To Compare
        </button>
      </div>
    )
  }
}

CharacterCard.propTypes = {
  id: PropTypes.string.isRequired,
  image: PropTypes.shape({
    url: PropTypes.string
  }).isRequired,
  name: PropTypes.string.isRequired,
  work: PropTypes.shape({
    occupation: PropTypes.string
  }).isRequired
}

const mapStateToProps = (state) => {
  return state.compare;
};

export default connect(mapStateToProps)(CharacterCard);
