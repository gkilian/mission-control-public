import CharacterCard from './CharacterCard';
import CharacterProfile from './CharacterProfile';

export {
  CharacterCard,
  CharacterProfile
};
