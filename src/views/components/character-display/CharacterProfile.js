import PropTypes from 'prop-types'
import React from 'react';

import './character-display.scss';

/**
 * CharacterProfile - Renders a full character profile
 *
 * @param {Object} props - React props
 * @param {Object} [props.appearance] - character traits
 * @param {string} [props.id] - character id
 * @param {Object} [props.image] - character profile image
 * @param {Object} [props.powerstats] - character stats
 * @param {string} [props.name] - character name
 * @param {Object} [props.work] - character occupation
 *
 * @returns {ReactElement} <div>
 */
const CharacterProfile = ({
  appearance,
  biography,
  id,
  image,
  powerstats,
  name,
  work
}) => {

  const {
    height,
    race,
    weight
  } = appearance;

  const powerStatNames = Object.keys(powerstats);

  return (
    <div className="character-profile">
      <div className="character-profile__image">
        <img alt={name} src={image.url} />
      </div>
      <aside className="character-profile__aside">
        <h1 className="character-profile__name">
          {name} ({biography['full-name']})
        </h1>
        <div className="character-profile__meta">
          <span><strong>Height:</strong> {height[0]}</span>
          <span><strong>Weight:</strong> {weight[0]}</span>
          <span><strong>Race:</strong> {race}</span>
        </div>
        <div className="character-profile__stats">
          <h3>Power Stats</h3>
          <table>
            <tbody>
              {powerStatNames.map(name => {
                return (
                  <tr key={name}>
                    <td className="power-name">{name}</td>
                    <td className="power-value">{powerstats[name]}</td>
                  </tr>
                );
              })}
            </tbody>
          </table>
        </div>
      </aside>
    </div>
  )
}

CharacterProfile.propTypes = {
  appearance: PropTypes.shape({
    height: PropTypes.array,
    race: PropTypes.string,
    weight: PropTypes.array
  }),
  biography: PropTypes.shape({
    'full-name': PropTypes.string
  }),
  id: PropTypes.string.isRequired,
  image: PropTypes.shape({
    url: PropTypes.string
  }),
  name: PropTypes.string.isRequired,
  powerstats: PropTypes.shape({
    combat: PropTypes.string,
    urability: PropTypes.string,
    intelligence: PropTypes.string,
    power: PropTypes.string,
    speed: PropTypes.string,
    strength: PropTypes.string
  }),
  work: PropTypes.shape({
    occupation: PropTypes.string
  })
}

export default CharacterProfile;
