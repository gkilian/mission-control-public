import classNames from 'classnames';
import React from 'react';
import { connect } from "react-redux";
import { Link } from "react-router-dom";

import { navigation } from '../../../config/navigation';
import { toggleNavigation } from '../../../store/actions';

import './navigation.scss';

/**
 * Navigation - Renders the main navigation
 *
 * @returns {ReactElement} <nav>
 */

const Navigation = ({ dispatch, isOpen }) => {
  return (
    <nav className={classNames('navigation', {'navigation--open': isOpen})}>
      <ul className="navigation__list">
        <li className="navigation__list-heading">Character Tools</li>
        {navigation.map((item, index) => {
          return (
            <li key={index} className="navigation__list-item">
              <Link to={item.url} onClick={() => dispatch(toggleNavigation())}>{item.title}</Link>
            </li>
          );
        })}
      </ul>
    </nav>
  )
}

const mapStateToProps = (state) => {
  return state.navigation;
};

export default connect(mapStateToProps)(Navigation);
