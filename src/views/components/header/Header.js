import React from 'react';
import { connect } from "react-redux";
import { Link } from "react-router-dom";

import { toggleNavigation } from '../../../store/actions';

import './header.scss';

/**
 * Header - Render the main site header
 *
 * @returns {ReactElement} <header>
 */

const Header = ({ dispatch }) => {
  return (
    <header className="header">
      <div className="header__menu-btn" onClick={() => dispatch(toggleNavigation())}>
        <img alt="checked" src={`${process.env.PUBLIC_URL}/menu.svg`} />
      </div>
      <h2 className="header__logo">
        <Link to="/">Mission Control</Link>
      </h2>
    </header>
  );
}

export default connect()(Header);
