import classNames from 'classnames';
import PropTypes from 'prop-types'
import React, { PureComponent } from 'react';
import { connect } from "react-redux";
import { Link, withRouter } from 'react-router-dom'

import './compare-drawer.scss';

/**
 * DrawerSlot - Renders a character slot for the drawer
 *
 * @param {Object} props - React props
 * @param {Object} props.character - character data
 *
 * @returns {ReactElement} <div>
 */
const DrawerSlot = ({ character }) => {
  return (
    <div className="compare-drawer__character-slot">
      <div className="compare-drawer__container">
        {character.length > 0 &&
          <React.Fragment>
            <img
              className="compare-drawer__image"
              alt={character[0].name}
              src={character[0].image.url}
            />
            <span className="compare-drawer__name">{character[0].name}</span>
          </React.Fragment>
        }
      </div>
    </div>
  )
}

/**
 * CompareDrawer - Renders a drawer with preview of hero vs. villain
 *
 * @param {Object} props - React props
 * @param {Object} props.location - location object from router
 * @param {Array} props.selectedHero - selected hero data
 * @param {Array} props.selectedVillain - selected villain data
 *
 * @returns {ReactElement} <div>
 */
class CompareDrawer extends PureComponent {
  render() {
    const { location, selectedHero, selectedVillain } = this.props;
    const containsHero = selectedHero.length > 0;
    const containsVillain = selectedVillain.length > 0;
    const drawerClassNames = classNames(
      'compare-drawer',
      {'compare-drawer--has-item': containsHero || containsVillain},
      {'compare-drawer--hidden': location.pathname === '/compare'}
    );

    return (
      <div className={drawerClassNames}>
        <h3>Compare Hero &amp; Villain:</h3>
        <DrawerSlot character={selectedHero} />
        <DrawerSlot character={selectedVillain} />
        {containsHero && containsVillain &&
          <Link className="compare-drawer__button btn" to="/compare">Compare</Link>
        }
      </div>
    )
  }
}

CompareDrawer.propTypes = {
  location: PropTypes.shape({
    pathname: PropTypes.string
  }),
  selectedHero: PropTypes.array,
  selectedVillain: PropTypes.array
}

const mapStateToProps = (state) => {
  return state.compare;
};

export default withRouter(connect(mapStateToProps)(CompareDrawer));
