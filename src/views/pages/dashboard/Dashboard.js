import PropTypes from 'prop-types'
import React, { Component } from 'react';
import { connect } from "react-redux";

import { getCharacters } from '../../../store/actions';

import CharacterGrid from '../../components/character-grid';

import './dashboard.scss';

/**
 * Dashboard - Renders the main dashboard page template
 *
 * @param {Object} props - React props
 * @param {Array} props.heros - Array of hero characters
 * @param {Array} props.villains - Array of villains characters
 *
 * @returns {ReactElement} <div>
 */

class Dashboard extends Component {
  constructor(props) {
    super(props);

    // Set path location to a readable slug
    this.template = props.location.pathname.replace('/', '');
  }

  componentDidMount() {
    const {
      dispatch,
      heros,
      villains
    } = this.props;

    // Dispatch events if the store is empty and we're in the right view
    if (villains.items.length < 1 || heros.items.length < 1) {
      dispatch(getCharacters(this.template))
    }
  }

  render() {
    const { heros, villains } = this.props;
    const characters = this.template === 'villains' ? villains.items : heros.items;
    const dashboardType = this.template === 'villains' ? 'Villains' : 'Heros'

    return (
      <div className="dashboard page--dashboard">
        <h1 className="title--callout dashboard__title">All {dashboardType}</h1>
        {!characters.length &&
          <img
            className="loader"
            alt="Loading"
            src={`${process.env.PUBLIC_URL}/loader.svg`}
          />
        }
        <CharacterGrid characters={characters} />
      </div>
    )
  }
}

Dashboard.propTypes = {
  heros: PropTypes.shape({
    items: PropTypes.array,
    selectedHero: PropTypes.array
  }),
  villains: PropTypes.shape({
    items: PropTypes.array,
    selectedVillain: PropTypes.array
  })
}

const mapStateToProps = state => state;

export default connect(mapStateToProps)(Dashboard);
