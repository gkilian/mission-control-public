import PropTypes from 'prop-types'
import React from 'react';
import { connect } from "react-redux";

import { CharacterProfile } from '../../components/character-display';

import './compare.scss';


/**
 * CompareSection - renders a section of the comparison window
 *
 * @param {Object} props - React props
 * @param {Object} props.character - current selected character
 *
 * @returns {ReactElement} <div>
 */

const CompareSection = ({ character, alignment }) => {
  return (
    <section className="character-section">
      <h3 className="title--callout character-section__title">{alignment}</h3>
      {character.length > 0 &&
        <CharacterProfile {...character[0]}/>
      }
      {character.length === 0 &&
        <div className="character-section__placeholder">
          <h3>No Character Selected</h3>
        </div>
      }
    </section>
  )
}

CompareSection.propTypes = {
  character: PropTypes.arrayOf(PropTypes.shape(CharacterProfile.propTypes)),
  alignment: PropTypes.oneOf(['Hero', 'Villain'])
}

/**
 * Compare - Renders comparison view of hero and villain
 *
 * @param {Object} props - React props
 * @param {Object} props.selectedHero - current selected hero
 * @param {Object} props.selectedVillain - current selected villain
 *
 * @returns {ReactElement} <div>
 */

const Compare = ({
  selectedHero,
  selectedVillain
}) => {
  return (
    <div className="compare page--compare">
      <div className="compare__container">
        <CompareSection character={selectedHero} alignment="Hero" />
        <CompareSection character={selectedVillain} alignment="Villain" />
      </div>
    </div>
  )
}

Compare.propTypes = {
  selectedHero: PropTypes.arrayOf(PropTypes.shape(CharacterProfile.propTypes)),
  selectedVillain: PropTypes.arrayOf(PropTypes.shape(CharacterProfile.propTypes))
}

const mapStateToProps = (state) => {
  return state.compare;
};

export default connect(mapStateToProps)(Compare);
