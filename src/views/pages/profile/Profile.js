import PropTypes from 'prop-types'
import React, { Component } from 'react';
import { connect } from "react-redux";

import { getCurrentCharacter } from '../../../store/actions';

import { CharacterProfile } from '../../components/character-display';

import './profile.scss';

/**
 * Profile - Renders the a character's detailed profile
 *
 * @param {Object} props - React props
 * @param {Array} props.currentCharacter - Current character data
 *
 * @returns {ReactElement} <div>
 */

class Profile extends Component {
  componentDidMount() {
    const { dispatch, match } = this.props;

    if (match.params) {
      dispatch(getCurrentCharacter(match.params.id));
    }
  }

  render() {
    const { currentCharacter } = this.props;
    return (
      <div className="profile page--profile">
        {!currentCharacter.length &&
          <img
            className="loader"
            alt="Loading"
            src={`${process.env.PUBLIC_URL}/loader.svg`}
          />
        }
        {!!currentCharacter.length &&
          currentCharacter.map(character => <CharacterProfile key={character.id} {...character}/>)
        }
      </div>
    )
  }
}

Profile.propTypes = {
  currentCharacter: PropTypes.array
}

const mapStateToProps = state => state;

export default connect(mapStateToProps)(Profile);
