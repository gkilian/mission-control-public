import { GET_HEROS } from '../actionTypes';

const initialState = {
  selectedHero: [],
  items: []
}

export default function heros(state = initialState, action) {
  switch (action.type) {
    case GET_HEROS:
      return Object.assign({}, state, {
        items: action.payload
      });
    default:
      return state;
  }
}
