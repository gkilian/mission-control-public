import { combineReducers } from 'redux'
import compare from './compare'
import currentCharacter from './currentCharacter';
import heros from './heros';
import navigation from './navigation';
import villains from './villains';

const rootReducer = combineReducers({
  compare,
  currentCharacter,
  heros,
  navigation,
  villains
})

export default rootReducer
