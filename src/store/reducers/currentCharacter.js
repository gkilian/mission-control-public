import { CLEAR_CURRENT_CHARACTER, GET_CURRENT_CHARACTER } from '../actionTypes';

export default function currentCharacter(state = [], action) {
  switch (action.type) {
    case GET_CURRENT_CHARACTER:
      return [action.payload];
    case CLEAR_CURRENT_CHARACTER:
      return [];
    default:
      return state;
  }
}
