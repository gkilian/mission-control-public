import { TOGGLE_NAVIGATION } from '../actionTypes';

const initialState = {
  isOpen: false
}

export default function compare(state = initialState, action) {
  switch (action.type) {
    case TOGGLE_NAVIGATION:
      return {
        isOpen: !action.payload
      }
    default:
      return state;
  }
}
