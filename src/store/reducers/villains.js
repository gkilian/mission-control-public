import { GET_VILLAINS } from '../actionTypes';

const initialState = {
  selectedVillain: [],
  items: []
}

export default function villains(state = initialState, action) {
  switch (action.type) {
    case GET_VILLAINS:
      return Object.assign({}, state, {
        items: action.payload
      });
    default:
      return state;
  }
}
