import { ADD_COMPARE_CHARACTER } from '../actionTypes';

const initialState = {
  selectedHero: [],
  selectedVillain: []
}

export default function compare(state = initialState, action) {
  switch (action.type) {
    case ADD_COMPARE_CHARACTER:
      const { biography } = action.payload;

      if (biography.alignment === 'good') {
        return Object.assign({}, state, {
          selectedHero: [action.payload]
        });
      }

      return Object.assign({}, state, {
        selectedVillain: [action.payload]
      });
    default:
      return state;
  }
}
