import axios from 'axios';
import { endpoints } from '../config/api';
import { heros, villains } from '../config/character-maps';
import {
  ADD_COMPARE_CHARACTER,
  CLEAR_CURRENT_CHARACTER,
  GET_CURRENT_CHARACTER,
  GET_HEROS,
  GET_VILLAINS,
  TOGGLE_NAVIGATION
} from './actionTypes';

export const getSuccess = (data, type) => {
  return {
    type,
    payload: data
  }
};

const getPromises = (set) => {
  const promises = [];

  set.forEach((item) => {
    promises.push(
      axios.get(`${endpoints.id}/${item.id}`, { mode: 'no-cors' })
    )
  });

  return promises;
}

function checkFailed () {
  return function (responses) {
    console.log();
    const someFailed = responses.some(response => response.data.error)

    if (someFailed) {
      throw new Error('There was an error fetching a character');
    }

    return responses
  }
}

export const getCharacters = (type) => {
  const characterMap = type === 'heros' ? heros : villains;
  const action = type === 'heros' ? GET_HEROS : GET_VILLAINS;

  return async function(dispatch) {
    const responseData = await axios.all(getPromises(characterMap))
      .then(checkFailed())
      .then((response) => {
        return response.map(payload => payload.data)
      })
      .catch((err) => {
        console.log('FAIL', err); // @todo: Handle the error on the FE
      });
    dispatch(getSuccess(responseData, action))
  }
}

export const getCurrentCharacter = (id) => {
  return async function(dispatch, getState) {
    const { currentCharacter } = getState();

    // Have the cached character and it's a match
    if (currentCharacter.length > 0 && currentCharacter[0].id === id) {
      return;
    } else if(currentCharacter.length > 0 && currentCharacter[0].id !== id) {
      // Have the cached character and it's not a match (clear)
      dispatch({type: CLEAR_CURRENT_CHARACTER})
    }

    // Handle Fresh page view || new request (fetch)
    const response = await axios.get(`${endpoints.id}/${id}`, { mode: 'no-cors' });
    dispatch(getSuccess(response.data, GET_CURRENT_CHARACTER))
  }
}

export const addCompareCharacter = (char) => {
  return function(dispatch) {
    dispatch(getSuccess(char, ADD_COMPARE_CHARACTER));
  }
}

export const toggleNavigation = () => {
  return function(dispatch, getState) {
    const { navigation } = getState();
    dispatch(getSuccess(navigation.isOpen, TOGGLE_NAVIGATION));
  }
};
