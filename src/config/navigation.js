export const navigation = [
  {
    title: 'View All Heros',
    url: '/heros'
  },
  {
    title: 'View All Villains',
    url: '/villains'
  },
  {
    title: 'Compare Hero & Villain',
    url: '/compare'
  }
];
