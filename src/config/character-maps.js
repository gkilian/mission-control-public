/**
 * Hero and Villain character map.
 * -----------------------------------------------------------------
 * For use with https://superheroapi.com/
 *
 * Reasons for the mappings
 * -----------------------------------------------------------------
 * 1. Current API doesn't have a way to query for all characters
 *    Or characters by there alignment
 * 2. We can only query the current API by id or name - that's only referenced
 *    on a static page.
 * 3. We don't want get out of hand and just query the API for all 731
 *    characters or to make sure we're retrieving the same number of character.
 *
 * @Todos:
 *  1. Create individual API endpoint that retrieves a full list and/or paginated
 *     lists of either Heros or Villains.
 *
 */

export const heros = [
  {
    id: 70,
    name: 'batman'
  },
  {
    id: 644,
    name: 'superman'
  },
  {
    id: 717,
    name: 'wolverine'
  },
  {
    id: 620,
    name: 'spider-man'
  },
  {
    id: 149,
    name: 'captain-america'
  },
  {
    id: 720,
    name: 'yoda'
  },
  {
    id: 527,
    name: 'professor-x'
  },
  {
    id: 332,
    name: 'hulk'
  },
  {
    id: 274,
    name: 'gambit'
  },
  {
    id: 346,
    name: 'iron-man'
  },
  {
    id: 567,
    name: 'rogue'
  },
  {
    id: 517,
    name: 'phoenix'
  },
];

export const villains = [
  {
    id: 423,
    name: 'magneto'
  },
  {
    id: 687,
    name: 'venom'
  },
  {
    id: 208,
    name: 'darth-vader'
  },
  {
    id: 655,
    name: 'thanos'
  },
  {
    id: 299,
    name: 'green-goblin'
  },
  {
    id: 370,
    name: 'joker'
  },
  {
    id: 60,
    name: 'bane'
  },
  {
    id: 273,
    name: 'galactus'
  },
  {
    id: 225,
    name: 'doctor-octopus'
  },
  {
    id: 570,
    name: 'sabretooth'
  },
  {
    id: 127,
    name: 'boba-fett'
  },
  {
    id: 480,
    name: 'mystique'
  },
]
