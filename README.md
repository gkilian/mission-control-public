# Mission Control App
___
This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Installation
**You’ll need to have Node 8.10.0 or later on your local development machine**

### Install modules

```sh
npm i
```

### Starting the development server

```sh
npm run
```

## Notes:
Below are some notes around my reasonings and things that just came up during development.

### API
This application uses the [SuperHero API](https://superheroapi.com/) which created numerous challenges:

- There aren't any endpoints to get all the characters or even to get a set of characters (heroes/villains)
- The API restricts you to only querying for characters using their `id` or `name`. There is no endpoint to get this information. Instead, you need to reference these value from a hard coded page on their website - [here](https://superheroapi.com/ids.html)

For these reasons I chose to create a map of characters (random and a limited set), broken up by their alignment (hero/villain).

**This gave me the ability to:**
1. Solve the problem of not having their `id` or `name`.
2. Allowed me to differentiate between their alignment.
3. Only fetch a small set of characters, knowing I'll return the desired amount of items.

**Possible better approaches/ideas that weren't done**
- Have a solid API in place.
- I could have attempted to batch fetch sets of characters (Being that I know the total amount of characters the API contains) and storing a global pagination variable so I can refetch consecutive sets there after. However, this approach **couldn't be trusted** as I may receive an unknown amount of characters from either alignment. Thus I would need to fetch more than I wanted, hoping to then have the correct number of positive hits. **Note:** The requirements stated that I can not commingle characters of different alignments. So having different buckets was a must.
